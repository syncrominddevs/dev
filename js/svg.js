var rotated = 0;
var rotateFunctionId;
function rotate(element, rotationDeg) {
  rotationDeg = Math.round(rotationDeg);
  console.log(rotated, rotationDeg);
  if((rotated == 0 && rotationDeg == 360) || rotated == rotationDeg)  return;
  if(Math.abs(rotated - rotationDeg) < 5) return;

  if(rotationDeg <= rotated)
    rotated -= 360;
  rotateFunctionId = setInterval(rotateElement, 0, element, rotationDeg);
}

function rotateElement(element, finalAngle) {
  rotated += 4;
  element.style.transform = "translate(-50%, -50%) rotate(" + rotated + "deg)";
  if(rotated >= finalAngle)
    clearInterval(rotateFunctionId);
}

function prueba(data){
	var url = data.attributes["url"].nodeValue;
	var assets = document.querySelector('a-assets')
	var asset = document.createElement('img');
	asset.setAttribute('id', 'source');
	asset.setAttribute('src', 'demo1.jpg');
	asset.setAttribute('crossorigin', 'anonymous');
	assets.appendChild(asset);
	document.querySelector('a-sky').setAttribute('src', url);
}

var Doughnut = function(options){
  this.options = options;
  this.svg = options.svg;

  this.draw = function(){
    var obj = this;
    var rectBounds = obj.svg.getBoundingClientRect();
    console.log(rectBounds);
    var total_value = 0;
    var radius = Math.min(rectBounds.width / 2, rectBounds.height/2);
    var sliceWidth = 100;
    var x0 = rectBounds.width / 2;
    var y0 = rectBounds.height / 2;

    total_value += obj.options.data.length * sliceWidth;

    var slice_angle = 2 * Math.PI * sliceWidth / total_value;
    var start_angle = Math.PI * 3 / 2 - slice_angle / 2;
    obj.options.data.forEach(function (cat, index) {
      obj.drawSlice(
        x0,
        y0, 
        radius,
        obj.options.radiusInner,
        start_angle,
        start_angle + slice_angle,
        cat.text,
		cat.url
      );

      start_angle += slice_angle;
    });
  };

  this.drawSlice = function(centerX, centerY, radiusOuter, radiusInner, startAngle, endAngle, label, url) {
    var obj = this;

    var path = document.createElementNS('http://www.w3.org/2000/svg', 'path');

    var separation = 5;
    var angleSepOuter = Math.asin(separation / (2 * radiusOuter)) * 2;
    var angleSepInner = Math.asin(separation / (2 * radiusInner)) * 2;

    var xInnerStart = centerX + (Math.cos(startAngle + angleSepInner) * radiusInner);
    var yInnerStart = centerY + (Math.sin(startAngle + angleSepInner) * radiusInner);
    var xInnerFinal = centerX + (Math.cos(endAngle - angleSepInner) * radiusInner);
    var yInnerFinal = centerY + (Math.sin(endAngle - angleSepInner) * radiusInner);

    var xOutterStart = centerX + (Math.cos(startAngle + angleSepOuter) * radiusOuter);
    var yOutterStart = centerY + (Math.sin(startAngle + angleSepOuter) * radiusOuter);
    var xOutterFinal = centerX + (Math.cos(endAngle - angleSepOuter) * radiusOuter);
    var yOutterFinal = centerY + (Math.sin(endAngle - angleSepOuter) * radiusOuter);

    var startAngleDeg = startAngle * 180 / Math.PI;
    var piece = `M ${xInnerStart}, ${yInnerStart} L ${xOutterStart}, ${yOutterStart} A ${radiusOuter} ${radiusOuter}, ${startAngleDeg}, 0, 1, ${xOutterFinal} ${yOutterFinal} L ${xInnerFinal}, ${yInnerFinal} A ${radiusInner} ${radiusInner}, ${startAngleDeg}, 0, 0, ${xInnerStart} ${yInnerStart}`;
    path.setAttribute("d", piece);
    path.setAttribute("fill", "rgba(255, 255, 255, 0.5)");
    path.style.cursor = "pointer";
	//path.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", "sssssss");
	path.setAttributeNS("", "url", url);
	path.setAttributeNS("", "onclick", "prueba(this);");
	//path.setAttribute("url", "dddddddd"); 
    //path.setAttribute("stroke", "red");
    obj.svg.appendChild(path);

    // Defines base arc where text will be written
    var radiusText = (radiusOuter + radiusInner) / 2;
    var xLabelStart = centerX + (Math.cos(startAngle) * radiusText);
    var yLabelStart = centerY + (Math.sin(startAngle) * radiusText);
    var xLabelFinal = centerX + (Math.cos(endAngle) * radiusText);
    var yLabelFinal = centerY + (Math.sin(endAngle) * radiusText);
    var labelArc = `M ${xLabelStart}, ${yLabelStart} A ${radiusText} ${radiusText}, ${startAngleDeg}, 0, 1, ${xLabelFinal} ${yLabelFinal}`;
    var defs = obj.svg.querySelector("defs");
    var pathDef = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    pathDef.setAttribute("id", label);
    pathDef.setAttribute("d", labelArc);
	//pathDef.setAttribute("url", "dddddddd"); 
	defs.appendChild(pathDef);

    // Put the text going in an arc shape, based on previous arc
    var text = document.createElementNS('http://www.w3.org/2000/svg', 'text');
    text.style.fontSize = "18px";
    text.setAttribute("fill", "#222");
    //text.setAttribute("stroke", "#222");
    text.style.cursor = "pointer";
    var textPath = document.createElementNS('http://www.w3.org/2000/svg', 'textPath');  
    textPath.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", `#${label}`);
    textPath.setAttribute("startOffset", "50%"); 
	textPath.setAttribute("url", url); 
    textPath.setAttribute("text-anchor", "middle");
	textPath.setAttribute("onclick", "prueba(this);");
    textPath.innerHTML = label;
    text.appendChild(textPath);
    obj.svg.appendChild(text);

    // Click and MouseOver events
    path.addEventListener("click", function() {
      obj.onMouseclick(obj.svg, startAngle, endAngle);
    });
    text.addEventListener("click", function() {
      obj.onMouseclick(obj.svg, startAngle, endAngle);
    });
    path.addEventListener("mouseover", function() {
      obj.onMouseover(path, text);
    });
    text.addEventListener("mouseover", function() {
      obj.onMouseover(path, text);
    });
    path.addEventListener("mouseout", function() {
      obj.onMouseout(path, text);
    });
    text.addEventListener("mouseout", function() {
      obj.onMouseout(path, text);
    });

  };

  this.onMouseclick = function (svg, startAngle, endAngle) {
	var startAngleDeg = startAngle * 180 / Math.PI;
    var endAngleDeg = endAngle * 180 / Math.PI;
    var sliceAngleDeg = endAngleDeg - startAngleDeg;
    var countSlices = 360 / sliceAngleDeg;
    var initialAngle = 270 - sliceAngleDeg / 2;
    var positionSlice = (startAngleDeg - initialAngle) / sliceAngleDeg;
    var remainingPositions = countSlices - positionSlice;
    rotate(svg, remainingPositions * sliceAngleDeg);
  };

  this.onMouseover = function (path, text) {
    path.setAttribute("stroke", "white");
    text.style.fontWeight = "bold";
  };

  this.onMouseout = function (path, text) {
    path.setAttribute("stroke", "transparent");
    text.style.fontWeight = "normal";
  };
}

var svg = document.getElementById("controller");

var slices = [
  {
    text: "Main",
	url: "assets/low/1.jpg"
  },
  {
    text: "Restaurant",
	url: "assets/low/2.jpg"
  },
  {
    text: "Amenities",
	url: "assets/low/3.jpg"
  },
  {
    text: "Rooms",
	url: "assets/low/4.jpg"
  },
  {
    text: "Abc",
	url: "assets/low/5.jpg"
  }
];

var d = new Doughnut(
  {
    svg: svg,
    data: slices,
    radiusInner: 55
  }
);
d.draw();

/*var rectBounds = svg.getBoundingClientRect();
drawDoughnutSlice(svg, rectBounds.width / 2, rectBounds.height / 2, Math.min(rectBounds.width / 2, rectBounds.height / 2), 50, 0, Math.PI / 2, "");*/
