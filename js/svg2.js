var rotated = 0;
var rotateFunctionId;

function rotate(element, rotationDeg) {
  rotationDeg = Math.round(rotationDeg);
  //console.log(rotated, rotationDeg);
  if((rotated == 0 && rotationDeg == 360) || rotated == rotationDeg)  return;
  if(Math.abs(rotated - rotationDeg) < 5) return;

  if(rotationDeg <= rotated)
    rotated -= 360;
  rotateFunctionId = setInterval(rotateElement, 0, element, rotationDeg);
}

function rotateElement(element, finalAngle) {
  rotated += 4;
  //console.log(element);
  element.style.transform = "translate(-50%, -50%) rotate(" + rotated + "deg)";
  if(rotated >= finalAngle)
    clearInterval(rotateFunctionId);
}

function findParentId(id, currentNode, currentParentId=0)
{
    var parentId = false;

    $.each(currentNode, function( key, cat )
    {
        if (id == cat.id)
        {
            parentId = currentParentId;
            return false;
        }

        if (typeof cat.items !== "undefined")
        {
            if (cat.items.length > 0) 
            {
                parentId = findParentId(id, cat.items, cat.id);

                if (parentId !== false) {
                    return false;
                }
            }
        }       
    });

    return parentId;  
}

function findNode(id, currentNode) 
{
    var result = false;

    $.each(currentNode, function( key, cat )
    {
        if (id == cat.id)
        {
            result = currentNode;
            return false;
        }

        if (typeof cat.items !== "undefined")
        {
            if (cat.items.length > 0) 
            {
                result = findNode(id, cat.items);

                if (result !== false) {
                    return false;
                }
            }
        }       
    });

    return result;
}

function back(id)
{
	parent_id = id;
	console.log(id);
    if (id == 0)
    {
		/*var options = { direction: "left" };
		$("#cont").toggle("slide", options, 500, function(){
			$("#visualizer-link").show();	
		});
		
        return;*/
    }

    var items = slices;

    var parentId = findParentId(id, items);
    var nodes = findNode(id, items);

    prueba('', nodes, [], parentId);
}

function prueba2(url) 
{
    document.querySelector('a-sky').setAttribute('src', url);
}

function prueba(data, items, links, parentId) 
{
    var url = '#';

    if (typeof data.attributes !== "undefined")
    {
        url = data.attributes["url"].nodeValue;
    }

    var ascene = document.querySelector('a-scene');
    var ascene_circle = ascene.querySelectorAll('a-circle');
    if (ascene_circle && ascene_circle.length > 0) {
        ascene_circle.forEach(function(el) {
            ascene.removeChild(el);
        });
    }

    if (url !== '#')
    {
        if (links.length > 0)
        {
            links.forEach(function(link) 
            {
                var circle = document.createElement('a-circle');

                circle.setAttribute('color', 'red');
                circle.style.cursor = "pointer";
          
                circle.setAttribute('position', `${link.position.x} ${link.position.y} ${link.position.z}`);
                circle.setAttribute('rotation', `${link.rotation.x} ${link.rotation.y} ${link.rotation.z}`);
                circle.setAttribute('radius', '1');
                circle.setAttribute('scale', '2 2 2');
                if (typeof link.url !== "undefined")
                {
                    circle.addEventListener('click', function () {
                        prueba2(link.url);
                      });
                }
                ascene.appendChild(circle);
            });
        }   

        var assets = document.querySelector('a-assets')
        var asset = document.createElement('img');
        asset.setAttribute('id', 'source');
        asset.setAttribute('src', 'demo1.jpg');
        asset.setAttribute('crossorigin', 'anonymous');
        assets.appendChild(asset);
        document.querySelector('a-sky').setAttribute('src', url);
    }
    

    if (items.length > 0)
    {
        var svg = document.getElementById("controller");
        svg.style = '';
        svg.innerHTML = '';
        var defs = document.createElement('defs');
        svg.appendChild(defs);

        var d = new Doughnut(
            {
                svg: svg,
                data: items,
                parentId: parentId,
                radiusInner: 55
            }
        );
        d.draw();
    }
}

var Doughnut = function(options)
{
    this.options = options;
    this.svg = options.svg;

    this.draw = function(){
        var obj = this;
        var rectBounds = obj.svg.getBoundingClientRect();
    
        var total_value = 0;
        var radius = Math.min(rectBounds.width / 2, rectBounds.height/2);
        var sliceWidth = 100;
        var x0 = rectBounds.width / 2;
        var y0 = rectBounds.height / 2;

        total_value += obj.options.data.length * sliceWidth;

        var slice_angle = 2 * Math.PI * sliceWidth / total_value;
        var start_angle = Math.PI * 3 / 2 - slice_angle / 2;
        var url = '#';
        obj.options.data.forEach(function (cat, index) 
        {
            url = (cat.show === false) ? '#' : cat.url;

            obj.drawSlice(
                x0,
                y0, 
                radius,
                obj.options.radiusInner,
                start_angle,
                start_angle + slice_angle,
                cat.name,
                url,
                cat.links,
                cat.items,
                cat.id
            );
            start_angle += slice_angle;
        });
        
        obj.drawCenter(
            150,
            150, 
            100,
            'Back',
            obj.options.parentId
        );
    };

    this.drawSlice = function(centerX, centerY, radiusOuter, radiusInner, startAngle, endAngle, label, url='#', links=[], items=[], parentId=0) {
        var obj = this;

        var path = document.createElementNS('http://www.w3.org/2000/svg', 'path');

        var separation = 5;
        var angleSepOuter = Math.asin(separation / (2 * radiusOuter)) * 2;
        var angleSepInner = Math.asin(separation / (2 * radiusInner)) * 2;

        var xInnerStart = centerX + (Math.cos(startAngle + angleSepInner) * radiusInner);
        var yInnerStart = centerY + (Math.sin(startAngle + angleSepInner) * radiusInner);
        var xInnerFinal = centerX + (Math.cos(endAngle - angleSepInner) * radiusInner);
        var yInnerFinal = centerY + (Math.sin(endAngle - angleSepInner) * radiusInner);

        var xOutterStart = centerX + (Math.cos(startAngle + angleSepOuter) * radiusOuter);
        var yOutterStart = centerY + (Math.sin(startAngle + angleSepOuter) * radiusOuter);
        var xOutterFinal = centerX + (Math.cos(endAngle - angleSepOuter) * radiusOuter);
        var yOutterFinal = centerY + (Math.sin(endAngle - angleSepOuter) * radiusOuter);

        var startAngleDeg = startAngle * 180 / Math.PI;
        var piece = `M ${xInnerStart}, ${yInnerStart} L ${xOutterStart}, ${yOutterStart} A ${radiusOuter} ${radiusOuter}, ${startAngleDeg}, 0, 1, ${xOutterFinal} ${yOutterFinal} L ${xInnerFinal}, ${yInnerFinal} A ${radiusInner} ${radiusInner}, ${startAngleDeg}, 0, 0, ${xInnerStart} ${yInnerStart}`;
        path.setAttribute("d", piece);
        path.setAttribute("fill", "rgba(255, 255, 255, 0.5)");
        path.style.cursor = "pointer";
        //path.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", "sssssss");
        path.setAttributeNS("", "url", url);
        path.setAttributeNS("", "onclick", "prueba(this,"+ JSON.stringify(items) +","+ JSON.stringify(links) +","+ parentId +");");
        //path.setAttribute("url", "dddddddd"); 
        //path.setAttribute("stroke", "red");
        obj.svg.appendChild(path);

        // Defines base arc where text will be written
        var radiusText = (radiusOuter + radiusInner) / 2;
        var xLabelStart = centerX + (Math.cos(startAngle) * radiusText);
        var yLabelStart = centerY + (Math.sin(startAngle) * radiusText);
        var xLabelFinal = centerX + (Math.cos(endAngle) * radiusText);
        var yLabelFinal = centerY + (Math.sin(endAngle) * radiusText);
        var labelArc = `M ${xLabelStart}, ${yLabelStart} A ${radiusText} ${radiusText}, ${startAngleDeg}, 0, 1, ${xLabelFinal} ${yLabelFinal}`;
        var defs = obj.svg.querySelector("defs");
        var pathDef = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        pathDef.setAttribute("id", label);
        pathDef.setAttribute("d", labelArc);
        //pathDef.setAttribute("url", "dddddddd"); 
        defs.appendChild(pathDef);

        // Put the text going in an arc shape, based on previous arc
        var text = document.createElementNS('http://www.w3.org/2000/svg', 'text');
        text.style.fontSize = "18px";
        text.setAttribute("fill", "#222");
        //text.setAttribute("stroke", "#222");
        text.style.cursor = "pointer";
        var textPath = document.createElementNS('http://www.w3.org/2000/svg', 'textPath');  
        textPath.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", `#${label}`);
        textPath.setAttribute("startOffset", "50%"); 
        textPath.setAttribute("url", url); 
        textPath.setAttribute("text-anchor", "middle");
        textPath.setAttribute("onclick", "prueba(this, "+ JSON.stringify(items) +","+ JSON.stringify(links) +","+ parentId +");");
        textPath.innerHTML = label;
        text.appendChild(textPath);
        obj.svg.appendChild(text);

        // Click and MouseOver events
        path.addEventListener("click", function() {
            obj.onMouseclick(obj.svg, startAngle, endAngle);
        });
        text.addEventListener("click", function() {
            obj.onMouseclick(obj.svg, startAngle, endAngle);
        });
        path.addEventListener("mouseover", function() {
            obj.onMouseover(path, text);
        });
        text.addEventListener("mouseover", function() {
            obj.onMouseover(path, text);
        });
        path.addEventListener("mouseout", function() {
            obj.onMouseout(path, text);
        });
        text.addEventListener("mouseout", function() {
            obj.onMouseout(path, text);
        });
    }; 

    this.drawCenter = function(centerX, centerY, radius, label, parentId) {
        var obj = this;
		
        var svg_center = document.getElementById("controller_center");
        svg_center.innerHTML = '';

        var defs = document.createElement('defs');
        svg_center.appendChild(defs);         

        var circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
        circle.setAttribute("cx", centerX);
        circle.setAttribute("cy", centerY);
        circle.setAttribute("r", radius);
        circle.setAttribute("fill", "rgba(255, 255, 255, 0.5)");
        circle.style.cursor = "pointer";
        circle.setAttributeNS("", "onclick", "back("+ parentId +");");
        svg_center.appendChild(circle);

        //var defs = svg2.querySelector("defs");
        var pathDef = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        pathDef.setAttribute("id", label);
        pathDef.setAttribute("d", "M 100.5, 165 A 0 0, 0, 0, 1, 190.5 165");
        defs.appendChild(pathDef);

        // Put the text going in an arc shape, based on previous arc
        var text = document.createElementNS('http://www.w3.org/2000/svg', 'text');
        text.style.fontSize = "50px";
        text.setAttribute("fill", "#222");
        text.style.cursor = "pointer";
        
        var textPath = document.createElementNS('http://www.w3.org/2000/svg', 'textPath');  
        textPath.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", `#${label}`);
        textPath.setAttribute("startOffset", "50%"); 
        textPath.setAttribute("text-anchor", "middle");
        textPath.setAttribute("onclick",  "back("+ parentId +");");
        textPath.innerHTML = label;
        text.appendChild(textPath);
        svg_center.appendChild(text);

        // Click and MouseOver events
        circle.addEventListener("click", function() {
            obj.onMouseclick2(svg_center);
        });
        text.addEventListener("click", function() {
            obj.onMouseclick2(svg_center);
        });
        circle.addEventListener("mouseover", function() {
            obj.onMouseover(circle, text);
        });
        text.addEventListener("mouseover", function() {
            obj.onMouseover(circle, text);
        });
        circle.addEventListener("mouseout", function() {
            obj.onMouseout(circle, text);
        });
        text.addEventListener("mouseout", function() {
            obj.onMouseout(circle, text);
        });
    };

    this.onMouseclick2 = function (svg) {
        //console.log(svg);
    };

    this.onMouseclick = function (svg, startAngle, endAngle) {
        var startAngleDeg = startAngle * 180 / Math.PI;
        var endAngleDeg = endAngle * 180 / Math.PI;
        var sliceAngleDeg = endAngleDeg - startAngleDeg;
        var countSlices = 360 / sliceAngleDeg;
        var initialAngle = 270 - sliceAngleDeg / 2;
        var positionSlice = (startAngleDeg - initialAngle) / sliceAngleDeg;

        var remainingPositions = countSlices - positionSlice;
        rotate(svg, remainingPositions * sliceAngleDeg);
    };

    this.onMouseover = function (path, text) {
        path.setAttribute("stroke", "white");
        text.style.fontWeight = "bold";
    };

    this.onMouseout = function (path, text) {
        path.setAttribute("stroke", "transparent");
        text.style.fontWeight = "normal";
    };
}

var svg = document.getElementById("controller");

/*var slices = [
  {
    text: "Main",
	url: "assets/low/1.jpg"
  },
  {
    text: "Restaurant",
	url: "assets/low/2.jpg"
  },
  {
    text: "Amenities",
	url: "assets/low/3.jpg"
  },
  {
    text: "Rooms",
	url: "assets/low/4.jpg"
  },
  {
    text: "Abc",
	url: "assets/low/5.jpg"
  }
];*/

var slices = [
    {
        id: 1,
        name: "Lobby",
        
        items: [
            {
                id: 11,
                name: 'Lobby 1',
                url: "assets/low/1.jpg",
                show: true
            },
            {
                id: 12,
                name: 'Lobby 2',
                url: "assets/low/2.jpg",
                show: true,
                links: [
                    {
                        link_id: 13,
                        position: {
                            x: 1.26745,
                            y: -34.34345,
                            z: 0
                        },
                        rotation: {
                            x: -10,
                            y: 5.67,
                            z: 0                            
                        }
                    }
                ]
            },
            {
                id: 13,
                name: 'Lobby 2 link 1',
                url: "assets/low/3.jpg",
                show: false
            },
            {
                id: 14,
                name: 'Lobby 2 link 2',
                url: "assets/low/4.jpg",
                show: false
            }
        ]
    },
    {
        id: 2,
        name: "Rooms",
        items: [
            {
                id: 21,
                name: 'Room 1',
                url: "assets/low/1.jpg",
                show: true
            },
            {
                id: 22,
                name: 'Room 2',
                url: "assets/low/2.jpg",
                show: true,
                links: [
                    {
                        link_id: 23,
                        position: {
                            x: 4,
                            y: 3,
                            z: -6
                        },
                        rotation: {
                            x: 0,
                            y: 45,
                            z: 45                           
                        },
                        url: "assets/low/4.jpg",
                    },
                    {
                        link_id: 23,
                        position: {
                            x: 2,
                            y: 1,
                            z: -6
                        },
                        rotation: {
                            x: 0,
                            y: 45,
                            z: 45                           
                        },
                        url: "assets/low/3.jpg",
                    }                    
                ]
            },
            {
                id: 23,
                name: 'Room 2 link 1',
                url: "assets/low/3.jpg",
                show: false
            },
            {
                id: 24,
                name: 'Room 2 link 2',
                url: "assets/low/4.jpg",
                show: false,
                items: [
                    {
                        id: 241,
                        name: 'Room 2 link 2 1',
                        url: "assets/low/3.jpg",
                        show: true
                    },
                    {
                        id: 242,
                        name: 'Room 2 link 2 2',
                        url: "assets/low/4.jpg",
                        show: true
                    }      
                ]               
            }       
        ]
    },
    {
        id: 3,
        name: 'Bathrooms',
        url: "assets/low/3.jpg",
        items: [
            {
                id: 31,
                name: 'Bathroom 1',
                url: "assets/low/3.jpg",
                show: true
            },
            {
                id: 32,
                name: 'Bathroom 2',
                url: "assets/low/4.jpg",
                items: [
                    {
                        id: 312,
                        name: 'Bathroom 1-1',
                        url: "assets/low/3.jpg",
                        items: [
                            {
                                id: 3121,
                                name: 'Bathroom 1-1-1',
                                url: "assets/low/3.jpg",
                                show: true
                            },
                            {
                                id: 3222,
                                name: 'Bathroom 2-2-1',
                                url: "assets/low/4.jpg",
                                show: true
                            }      
                        ]
                    },
                    {
                        id: 322,
                        name: 'Bathroom 2-2',
                        url: "assets/low/4.jpg",
                        show: true
                    }      
                ]
            }      
        ]
    },
    {
        id: 4,
        name: 'Kitchen',
        url: "assets/low/4.jpg",
        show: true
    }
];

var d = new Doughnut(
  {
    svg: svg,
    data: slices,
    parentId: 0,
    radiusInner: 55
  }
);
d.draw();

/*var rectBounds = svg.getBoundingClientRect();
drawDoughnutSlice(svg, rectBounds.width / 2, rectBounds.height / 2, Math.min(rectBounds.width / 2, rectBounds.height / 2), 50, 0, Math.PI / 2, "");*/
